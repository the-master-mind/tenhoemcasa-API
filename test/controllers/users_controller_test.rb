require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:user_one)
    @user_token = JsonWebToken.encode(user_id: @user.id)
    @user2 = users(:user_two)
    @new_user = User.create(name: 'John Doe',
                            email: 'johndoe@email.com',
                            password: 'safest-pass-ever')
  end

  test 'should create/register user' do
    post register_path, params: {
      user: {
        name: 'new user',
        email: 'newuser@email.com',
        password: 'new-user-pass'
      }
    }, as: :json
    assert_response 201
  end

  test 'should not create a user with missing parameters' do
    post register_path, params: { user: { name: @user.name } }, as: :json
    assert_match /can't be blank/, response.body

    post register_url, params: { user: { password: @user2.password } }, as: :json
    assert_match /can't be blank/, response.body
  end

  test 'should show user' do
    get user_url(@user), headers: { 'Authorization' => @user_token }, as: :json
    assert_response :success
  end

  test 'should not show user without authorization token' do
    get user_url(@user), as: :json
    assert_match /Nil JSON web token/, response.body
  end

  test 'should not show user with invalid token' do
    get user_url(@user), headers: { 'Authorization' => 'invalid-token' }, as: :json

    assert_match /Not enough or too many segments/, response.body
  end

  test 'should not show user with expired token' do
    JWT.stubs(:decode).raises(JWT::ExpiredSignature)

    get user_url(@user), headers: { 'Authorization' => 'a-expired-token' }, as: :json

    assert_match /JWT::ExpiredSignature/, response.body
  end

  test 'should update user' do
    patch user_url(@new_user), headers: { 'Authorization' => @user_token }, params: {
      user: {
        name: 'Updated Name',
        email: @new_user.email,
        password: @new_user.password
      }
    }, as: :json
    assert_response 200
  end

  test 'should log a user with correct credentials in' do
    login_user = User.create!(name: 'Valid Name',
                              email: 'valid-email@email.com',
                              password: 'valid-password')

    post login_url, params: {
      user: {
        email: login_user.email,
        password: login_user.password
      }
    }, as: :json
    assert_response :ok
    token = JSON.parse(response.body)['token']
    assert_equal login_user.id, JsonWebToken.decode(token)['user_id']
  end

  test 'should not log a user with incorrect credentials in' do
    login_user = User.create(name: @new_user.name,
                             email: @new_user.email,
                             password: @new_user.password)
    post login_url, params: {
      user: {
        email: login_user.email,
        password: 'wrong-pass'
      }
    }, as: :json
    assert_response :unauthorized
  end

  test 'should destroy user' do
    assert_difference('User.count', -1) do
      delete user_url(@user), headers: { 'Authorization' => @user_token }, as: :json
    end
    assert_response 204
  end
end
