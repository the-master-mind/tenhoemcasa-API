require 'test_helper'

class RecipesControllerTest < ActionDispatch::IntegrationTest
  # TODO: Test requests without token
  setup do
    @user = users(:user_one)
    @token = JsonWebToken.encode(user_id: @user.id)
    @recipe = recipes(:one)
  end

  test 'should get index' do
    get recipes_url, headers: { 'Authorization' => @token }, as: :json
    assert_response :success
  end

  test 'should create recipe' do
    assert_difference('Recipe.count') do
      post recipes_url, headers: { 'Authorization' => @token }, params: {
        recipe: {
          instructions: @recipe.instructions,
          name: @recipe.name,
          preparation_time_in_minutes: @recipe.preparation_time_in_minutes,
          servings_amount: @recipe.servings_amount
        }
      }, as: :json
    end

    assert_response :created
  end

  test 'should not accept recipe without name' do
    assert_no_difference('Recipe.count') do
      post recipes_url, headers: { 'Authorization' => @token }, params: {
        recipe: {
          instructions: @recipe.instructions,
          name: '',
          preparation_time_in_minutes: @recipe.preparation_time_in_minutes,
          servings_amount: @recipe.servings_amount
        }
      }, as: :json
    end
    assert_response :unprocessable_entity

    patch recipe_url(@recipe), headers: { 'Authorization' => @token }, params: {
      recipe: { name: '' }
    }, as: :json
    assert_response :unprocessable_entity
  end

  test 'should not accept recipe with name smaller than 3 chars' do
    assert_no_difference('Recipe.count') do
      post recipes_url, headers: { 'Authorization' => @token }, params: {
        recipe: {
          instructions: @recipe.instructions,
          name: '12',
          preparation_time_in_minutes: @recipe.preparation_time_in_minutes,
          servings_amount: @recipe.servings_amount
        }
      }, as: :json
    end
    assert_response :unprocessable_entity

    patch recipe_url(@recipe), headers: { 'Authorization' => @token }, params: {
      recipe: { name: '12' }
    }, as: :json
    assert_response :unprocessable_entity
  end

  test 'should not accept recipe without instructions' do
    assert_no_difference('Recipe.count') do
      post recipes_url, headers: { 'Authorization' => @token }, params: {
        recipe: {
          instructions: '',
          name: @recipe.name,
          preparation_time_in_minutes: @recipe.preparation_time_in_minutes,
          servings_amount: @recipe.servings_amount
        }
      }, as: :json
    end
    assert_response :unprocessable_entity

    patch recipe_url(@recipe), headers: { 'Authorization' => @token }, params: {
      recipe: { instructions: '' }
    }, as: :json
    assert_response :unprocessable_entity
  end

  test 'should show recipe' do
    get recipe_url(@recipe), headers: { 'Authorization' => @token }, as: :json
    assert_response :success
  end

  test 'should update recipe' do
    patch recipe_url(@recipe), headers: { 'Authorization' => @token }, params: {
      recipe: {
        instructions: @recipe.instructions,
        name: @recipe.name,
        preparation_time_in_minutes: @recipe.preparation_time_in_minutes,
        servings_amount: @recipe.servings_amount
      }
    }, as: :json
    assert_response :ok
  end

  test 'should destroy recipe' do
    assert_difference('Recipe.count', -1) do
      delete recipe_url(@recipe), headers: { 'Authorization' => @token }, as: :json
    end

    assert_response :no_content
  end

  test 'should create recipe with ingredients' do
    assert_difference('Recipe.count') do
      post recipes_url, headers: { 'Authorization' => @token }, params: {
        recipe: {
          instructions: 'My secred instructions.',
          name: 'My Secret Recipe',
          preparation_time_in_minutes: 10,
          servings_amount: 2,
          ingredient_ids: [ingredients(:one).id, ingredients(:two).id]
        }
      }, as: :json
    end

    assert_response :created
    assert_equal 2, Recipe.last.ingredients.count

    patch recipe_url(@recipe), headers: { 'Authorization' => @token }, params: {
      recipe: {
        ingredient_ids: [ingredients(:one).id, ingredients(:two).id]
      }
    }, as: :json
    @recipe.reload

    assert_response :ok
    assert_equal 2, @recipe.ingredients.count
  end
end
