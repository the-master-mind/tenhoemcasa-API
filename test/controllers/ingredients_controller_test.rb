require 'test_helper'

class IngredientsControllerTest < ActionDispatch::IntegrationTest
  # TODO: Test requests without token
  setup do
    @user = users(:user_one)
    @token = JsonWebToken.encode(user_id: @user.id)
    @ingredient = ingredients(:one)
  end

  test 'should get index' do
    get ingredients_url, headers: { 'Authorization' => @token }, as: :json
    assert_response :success
  end

  test 'should create ingredient' do
    assert_difference('Ingredient.count') do
      post ingredients_url, headers: { 'Authorization' => @token }, params: {
        ingredient: { name: 'New Ingredient' }
      }, as: :json
    end

    assert_response :created
  end

  test 'should not accept ingredient without name' do
    assert_no_difference('Ingredient.count') do
      post ingredients_url, headers: { 'Authorization' => @token }, params: {
        ingredient: { name: '' }
      }, as: :json
    end
    assert_response :unprocessable_entity

    patch ingredient_url(@ingredient), headers: { 'Authorization' => @token }, params: {
      ingredient: { name: '' }
    }, as: :json
    assert_response :unprocessable_entity
  end

  test 'should not accept duplicated ingredient' do
    assert_no_difference('Ingredient.count') do
      post ingredients_url, headers: { 'Authorization' => @token }, params: {
        ingredient: { name: @ingredient.name }
      }, as: :json
    end
    assert_response :unprocessable_entity

    other_ingredient = ingredients(:two)
    patch ingredient_url(other_ingredient), headers: { 'Authorization' => @token }, params: {
      ingredient: { name: @ingredient.name }
    }, as: :json
    assert_response :unprocessable_entity
  end

  test 'should show ingredient' do
    get ingredient_url(@ingredient), headers: { 'Authorization' => @token }, as: :json
    assert_response :success
  end

  test 'should update ingredient' do
    patch ingredient_url(@ingredient), headers: { 'Authorization' => @token }, params: {
      ingredient: { name: 'New Name' }
    }, as: :json
    assert_response :ok
  end

  test 'should destroy ingredient' do
    assert_difference('Ingredient.count', -1) do
      delete ingredient_url(@ingredient), headers: { 'Authorization' => @token }, as: :json
    end

    assert_response :no_content
  end
end
