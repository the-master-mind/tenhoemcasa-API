require 'test_helper'

class IngredientTest < ActiveSupport::TestCase
  setup do
    @ingredient = ingredients(:one)
  end

  test 'should save valid ingredient' do
    new_ingredient = Ingredient.new(name: 'New Ingredient')
    assert new_ingredient.save!
  end

  test 'should not save ingredient without name' do
    @ingredient.name = ''
    refute @ingredient.save
  end

  test 'should update ingredient name' do
    @ingredient.name = 'New Name'
    assert @ingredient.save
  end

  test 'should not save ingredient with an existing name' do
    new_ingredient = Ingredient.new(name: @ingredient.name)
    refute new_ingredient.save
  end

  test 'should add recipe to ingredient' do
    recipe = recipes(:one)
    @ingredient.recipes << recipe

    assert @ingredient.save
    assert_equal 1, @ingredient.recipes.count
  end

  test 'should update ingredient recipes' do
    ingredient = Ingredient.create!(name: 'Milk', recipes: [recipes(:one)])
    assert_equal 1, ingredient.recipes.count

    ingredient.recipes = []
    assert ingredient.save
    assert_empty ingredient.recipes

    ingredient.recipes = [recipes(:one), recipes(:two)]
    assert ingredient.save
    assert_equal 2, ingredient.recipes.count
  end

  test 'should downcase name before save' do
    new_ingredient = Ingredient.new(name: 'New Ingredient')
    assert new_ingredient.save!

    assert_equal 'new ingredient', new_ingredient.name
  end
end
