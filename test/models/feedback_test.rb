require 'test_helper'

class FeedbackTest < ActiveSupport::TestCase
  setup do
    @feedback = feedbacks(:one)
  end

  test 'should save valid feedback' do
    new_feedback = Feedback.new(kind: 'like')
    assert new_feedback.save!

    new_feedback = Feedback.new(kind: 'dislike', body: 'a' * 256)
    assert new_feedback.save!
  end

  test 'should not save feedback without kind' do
    @feedback.kind = ''
    refute @feedback.save
  end

  test 'should not save feedback with invalid kind' do
    @feedback.kind = 'invalid'
    refute @feedback.save
    assert @feedback.errors.messages[:kind].include?('invalid is not a valid type')
  end

  test 'should not save feedback with body greater than 256 chars' do
    @feedback.body = 'a' * 257
    refute @feedback.save
    assert @feedback.errors.messages[:body].include?('is too long (maximum is 256 characters)')
  end

  test 'should downcase kind before save' do
    new_feedback = Feedback.new(kind: 'LIKE', body: 'a' * 256)
    assert new_feedback.save!

    assert_equal 'like', new_feedback.kind
  end
end
