require 'test_helper'

class RecipeTest < ActiveSupport::TestCase
  setup do
    @recipe = recipes(:one)
  end

  test 'should save valid recipe' do
    new_recipe = Recipe.new(name: 'New recipe', instructions: 'Some instructions')
    assert new_recipe.save!
  end

  test 'should not save recipe without name' do
    @recipe.name = ''
    refute @recipe.save
  end

  test 'should not save recipe with name smaller than 3 chars' do
    @recipe.name = '12'
    refute @recipe.save
  end

  test 'should not save recipe without instructions' do
    new_recipe = Recipe.new(name: @recipe.name)
    refute new_recipe.save
  end

  test 'should update recipe name' do
    @recipe.name = 'New Name'
    assert @recipe.save
  end

  test 'should add ingredient to recipe' do
    ingredient = ingredients(:one)
    @recipe.ingredients << ingredient

    assert @recipe.save
    assert_equal 1, @recipe.ingredients.count
  end

  test 'should update recipe ingredients' do
    recipe = Recipe.create!(name: 'My Recipe', instructions: 'Nice Instructions',
                            ingredients: [ingredients(:one)])
    assert_equal 1, recipe.ingredients.count

    recipe.ingredients = []
    assert recipe.save
    assert_empty recipe.ingredients

    recipe.ingredients = [ingredients(:one), ingredients(:two)]
    assert recipe.save
    assert_equal 2, recipe.ingredients.count
  end
end
