require 'test_helper'

class UserTest < ActiveSupport::TestCase
  setup do
    @user = users(:user_one)
  end

  test 'should save a valid user' do
    assert @user.save!
  end

  test 'should not save invalid user' do
    wrong_user = User.new(name: nil, email: nil, password_digest: nil)
    refute wrong_user.save
  end

  test 'should not save user without name' do
    wrong_user = User.new(email: 'wrong-user@email.com', password_digest: 'safest-pass-ever')
    assert_raise do
      wrong_user.save!
    end
    error_message = { name: ["can't be blank"] }
    assert_equal error_message, wrong_user.errors.messages
  end

  test 'should not save user without email' do
    wrong_user = User.new(name: 'Wrong User Silva', password_digest: 'safest-pass-ever')
    assert_raise do
      wrong_user.save!
    end
    error_message = { email: ["can't be blank"] }
    assert_equal error_message, wrong_user.errors.messages
  end

  test 'should not save user without password' do
    wrong_user = User.new(name: 'Wrong User Silva', email: 'wrong-user@email.com')
    assert_raise do
      wrong_user.save!
    end
    error_message = { password_digest: ["can't be blank"], password: ["can't be blank"] }
    assert_equal error_message, wrong_user.errors.messages
  end

  test 'should not save user with a duplicated email' do
    wrong_user = User.new(name: 'Humble User', email: @user.email, password_digest: 'one-safe-pass')
    assert_raise do
      wrong_user.save!
    end
    error_message = { email: ['has already been taken'] }
    assert_equal error_message, wrong_user.errors.messages
  end
end
