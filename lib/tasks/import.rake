require 'csv'

RECIPES_PATH = Rails.root.join('db', 'import', 'receitas-v4.csv')
BATCH_SIZE = 100

def format_ingredients(ingredients)
  ingredients.downcase.gsub('"', '').split(',').map { |c| c.strip }.reject(&:empty?)
end

namespace :import do
  desc 'Imports ingredients from CSV'
  task ingredients: :environment do
    ingredients = []
    CSV.foreach(RECIPES_PATH, headers: true) do |row|
      unless row['ingredients'].blank?
        raw_data = format_ingredients(row['ingredients'])
        ingredients += raw_data
      end
    end

    ingredients.uniq!
    existing_ingredients = Ingredient.pluck(:name)
    ingredients -= existing_ingredients

    previous_ingredient_amount = Ingredient.count
    fails = 0
    ingredients.each_slice(BATCH_SIZE) do |batch|
      batch.map! { |a| Array(a) }
      result = Ingredient.import [:name], batch
      fails += result.failed_instances.count
      # pp result.failed_instances
    end
    ingredient_amount = Ingredient.count

    puts "[ IMPORT ] #{ingredient_amount - previous_ingredient_amount} Ingredients imported!"
    puts "[ IMPORT ] #{fails} failed imports!"
  end

  task recipes: :environment do
    ingredients_hash = Ingredient.pluck(:name, :id).to_h

    raw_recipes = []
    CSV.foreach(RECIPES_PATH, headers: true) do |row|
      unless row['ingredients'].blank?
        ingredients = format_ingredients(row['ingredients'])
        ingredient_ids = ingredients.map { |i| ingredients_hash[i] }

        # Get first number
        servings_amount = row['servings_amount'][/\d+/] unless row ['servings_amount'].blank?
        preparation_time = row['preparation_time'][/\d+/] unless row ['preparation_time'].blank?
        raw_recipes << Recipe.new(name: row['name'],
                                  instructions: row['instructions'],
                                  servings_amount: servings_amount,
                                  preparation_time_in_minutes: preparation_time,
                                  category: row['category'],
                                  ingredient_ids: ingredient_ids)
      end
    end

    existing_recipes = Recipe.pluck(:name, :instructions).to_h

    # Keep if there is not a recipe with same name and instructions
    recipes = raw_recipes.delete_if do |recipe|
      existing_recipes[recipe.name] == recipe.instructions
    end

    previous_recipe_amount = Recipe.count
    fails = 0
    recipes.each_slice(BATCH_SIZE) do |batch|
      result = Recipe.import batch, recursive: true
      fails += result.failed_instances.count
      pp result.failed_instances.map(&:errors).map(&:messages)
    end
    recipe_amount = Recipe.count

    puts "[ IMPORT ] #{recipe_amount - previous_recipe_amount} Recipes imported!"
    puts "[ IMPORT ] #{fails} failed imports!"
  end
end
