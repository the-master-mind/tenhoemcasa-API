FROM ruby:2.5.3-slim

RUN apt-get update -qq && \
    apt-get install -y nodejs \
                       build-essential \
                       libpq-dev

COPY . /tenhoemcasa-API

WORKDIR /tenhoemcasa-API

ENTRYPOINT ["sh","scripts/start.sh"]
