class CreateJoinTableIngredientRecipe < ActiveRecord::Migration[5.2]
  def change
    create_join_table :ingredient, :recipes do |t|
      t.index [:recipe_id, :ingredient_id]
      t.index [:ingredient_id, :recipe_id]
    end
  end
end
