class CreateRecipes < ActiveRecord::Migration[5.2]
  def change
    create_table :recipes do |t|
      t.string :name
      t.text :instructions
      t.integer :servings_amount
      t.integer :preparation_time_in_minutes
      t.string :category

      t.timestamps
    end
  end
end
