# This file should contain all the record creation needed to seed the database
# with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside
# the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def count_created_objects(model, &block)
  previous_number = model.count
  yield
  created_objects = model.all.count - previous_number
  puts "[SEED] Successfully created #{created_objects} #{model.to_s.pluralize(created_objects)}!"
end

count_created_objects(User) do
  (0..4).to_a.each do |index|
    User.find_or_create_by(name: "user-#{index}",
                           email: "user-#{index}@email.com",
                           password_digest: "user-#{index}-pass")
  end
end

count_created_objects(Ingredient) do
  ['100g of Cheese', '100g of Bread', '100g of Eggs', '100g of Tomato',
   '100g of Steak', '100g of Chocolate'].map { |name| Ingredient.create(name: name) }
end

count_created_objects(Recipe) do
  recipes = [
    {
      name: 'Egg Sandwich',
      instructions: "Crack the egg into a microwave-safe cereal bowl and whisk in the \
                     milk. Season with salt and pepper. Cook in the microwave on 100% \
                     power for 1 to 2 minutes, or until cooked through.",
      category: 'Salty'
    },
    {
      name: 'Stroganoff Steaks',
      instructions: "Fry the onion in the butter in a large frying pan until soft. \
                     Stir in the garlic and mushrooms and fry over high heat until \
                     browned.",
      category: 'Salty'
    },
    {
      name: 'Chocolate Cake',
      instructions: "Heat the oven to 190C/fan170C/gas 5 and line two baking sheets \
                     with non-stick baking paper. Put 150g softened salted butter, 80g \
                     light brown muscovado sugar and 80g granulated sugar into a bowl \
                     and beat until creamy.",
      category: 'Sweet'
    },
    {
      name: 'Chocolate Cookies',
      instructions: "Heat oven to 180C/160C fan/gas 4. Grease and line the base of two \
                     18cm sandwich tins. Sieve 175g self-raising flour, 2 tbsp cocoa \
                     powder and 1 tsp bicarbonate of soda into a bowl. Add 150g caster \
                     sugar and mix well.",
      category: 'Sweet'
    },
    {
      name: 'Apple Juice',
      instructions: "Simmer the apple juice with the strips of orange peel, cinnamon \
                     stick and cloves for about 5-10 mins until all the flavours have \
                     infused. Sweeten to taste.",
      category: 'Fitness'
    },
    {
      name: 'Greek Salad',
      instructions: "Place 4 large vine tomatoes, cut into wedges, 1 peeled, deseeded \
                     and chopped cucumber, ½ a thinly sliced red onion, 16 Kalamata \
                     olives, 1 tsp dried oregano, 85g feta cheese chunks and 4 tbsp \
                     Greek extra virgin olive oil in a large bowl.",
      category: 'Fitness'
    }
  ]

  recipes.each do |recipe|
    Recipe.create(name: recipe[:name],
                  instructions: recipe[:instructions],
                  ingredient_ids: Ingredient.all.sample(2).map(&:id),
                  servings_amount: rand(1..5),
                  preparation_time_in_minutes: rand(1..10) * 5,
                  category: recipe[:category])
  end
end
