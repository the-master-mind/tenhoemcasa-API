Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :ingredients
  resources :feedbacks
  resources :recipes
  resources :users
  post '/register', to: 'users#register'
  post '/login', to: 'users#login'
  post '/search', to: 'recipes#search'
end
