class Authentication
  def initialize(credentials)
    @email = credentials[:email]
    @password = credentials[:password]
  end

  attr_accessor :email, :password

  def generate_token
    JsonWebToken.encode(user_id: @user.id) if authenticable?
  end

  private

    def authenticable?
      @user = User.find_by_email(email)
      if @user && @user.authenticate(password)
        @user
      else
        nil
      end
    end
end
