class RecipesController < ApplicationController
  before_action :set_recipe, only: [:show, :update, :destroy]
  skip_before_action :authenticate, only: [:search, :show] # FIXME: REMOVE THIS

  # GET /recipes
  def index
    @recipes = Recipe.all

    render json: @recipes, except: [:created_at, :updated_at]
  end

  # GET /recipes/1
  def show
    render json: @recipe, include: [ingredients: { only: :name }], except: [:created_at, :updated_at]
  end

  # POST /recipes
  def create
    @recipe = Recipe.new(recipe_params)

    if @recipe.save
      render json: @recipe, status: :created, location: @recipe
    else
      render json: @recipe.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /recipes/1
  def update
    if @recipe.update(recipe_params)
      render json: @recipe
    else
      render json: @recipe.errors, status: :unprocessable_entity
    end
  end

  # DELETE /recipes/1
  def destroy
    @recipe.destroy
  end

  def search
    ingredients = params[:ingredients].map(&:downcase).join('|')

    query = ActiveRecord::Base.connection.execute(
      "WITH cte_ingredient AS (
        select recipe_id, count(0) as matched_ingredients_amount from (
               select * from ingredient_recipes as a inner join ingredients as b
                     on a.ingredient_id = b.id) as c WHERE name similar to
                     '%(#{ingredients})%' GROUP BY recipe_id
      ), cte_prod AS (
        select c.recipe_id, count(0) as ingredient_amount_on_recipe, d.matched_ingredients_amount from
               (select * from ingredient_recipes as a inner join ingredients as b
                on a.ingredient_id = b.id) as c
        INNER JOIN cte_ingredient as d
        ON d.recipe_id = c.recipe_id GROUP BY c.recipe_id, d.matched_ingredients_amount
      ) SELECT recipe_id, r.name, r.servings_amount, r.preparation_time_in_minutes,
               r.category, matched_ingredients_amount, ingredient_amount_on_recipe,
      CASE
      WHEN (matched_ingredients_amount = ingredient_amount_on_recipe) THEN 'total'
      WHEN ((ingredient_amount_on_recipe - matched_ingredients_amount) <= 3) THEN 'partial'
      END AS match
      FROM cte_prod
      INNER JOIN recipes AS r ON r.id = cte_prod.recipe_id
      WHERE matched_ingredients_amount = ingredient_amount_on_recipe
            OR (
              (ingredient_amount_on_recipe - matched_ingredients_amount) <= 3
              AND matched_ingredients_amount > 0
            )
      ORDER BY match DESC, matched_ingredients_amount DESC, name;"
    )

    render json: query
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def recipe_params
      params.require(:recipe).permit(:name, :instructions, :servings_amount,
                                     :preparation_time_in_minutes, :ingredients,
                                     ingredient_ids: [])
    end
end
