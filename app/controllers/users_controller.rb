class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  skip_before_action :authenticate, only: [:login, :register]

  def register
    @user = User.create(user_params)

    if @user.save
      result = { user_id: @user.id, name: @user.name, email: @user.email }
      render json: result, status: :created
    else
      render json: @user.errors, status: :bad
    end
  end

  def login
    authenticate_user(params[:email])
  end

  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def show
    if @user.id
      render json: { id: @user.id, name: @user.name }
    else
      render json: { error: 'Not Authorized' }, status: 401
    end
  end

  def destroy
    @user.destroy
  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :email, :password)
    end

    def authenticate_user(email)
      auth_object = Authentication.new(user_params)
      if auth_object.generate_token
        render json: {
          message: 'Successfully logged in', token: auth_object.generate_token
        }, status: :ok
      else
        render json: {
          message: 'Incorrect credentials'
        }, status: :unauthorized
      end
    end
end
