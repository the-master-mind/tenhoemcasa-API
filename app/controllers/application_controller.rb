class ApplicationController < ActionController::API
  include ExceptionHandler

  before_action :authenticate
  before_action :set_cors_headers

  private

    def authenticate
      @current_user = AuthorizationHeader.call(request.headers).result
      unless @current_user
        render json: { error: "Not Authorized! 'Authorization' header field not found." }, status: 401
      end
    end

    def set_cors_headers
      response.set_header 'Access-Control-Allow-Origin', origin
    end

    def origin
      request.headers['Origin'] || '*'
    end
end
