class Recipe < ApplicationRecord
  validates :name, presence: true, length: { minimum: 3 }
  validates :instructions, presence: true
  validates :category, inclusion: { in: ['sweet', 'salty', 'fitness'], allow_blank: true }
  before_validation { self.category.downcase! unless self.category.blank? }

  has_many :ingredient_recipes
  has_many :ingredients, through: :ingredient_recipes
end
