class Feedback < ApplicationRecord
  validates :kind, inclusion: { in: %w(like dislike), message: '%{value} is not a valid type' }
  validates :body, length: { maximum: 256 }
  before_validation { self.kind.downcase! }
end
