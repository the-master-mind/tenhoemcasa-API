class Ingredient < ApplicationRecord
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  before_save { self.name.downcase! }

  has_many :ingredient_recipes
  has_many :recipes, through: :ingredient_recipes # TODO: Destroy recipes on delete
end
