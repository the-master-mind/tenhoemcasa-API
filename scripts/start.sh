#!/bin/bash

bundle check || bundle install

postgres_ready() {
bundle exec rails db:create
ruby << END
require 'pg'
begin
  PG.connect(dbname: "$PG_DATABASE_DEV", user: "$PG_USERNAME", password: "$PG_PASSWORD", host: "$PG_HOST")
rescue
  exit -1
else
  exit 0
end
END
}

until postgres_ready; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 5
done

>&2 echo "Postgres is up - executing commands..."

if bundle exec rails db:exists; then
	bundle exec rails db:migrate
else 
	>&2 echo "Database doesn't exist, creating and migrating it..."
	bundle exec rails db:create
	bundle exec rails db:migrate
fi

if [ -f $pidfile ] ; then
	>&2 echo 'Server PID file already exists. Removing it...'
	rm $pidfile
fi

>&2 echo 'Running server...'
bundle exec rails s -p 3000 -b 0.0.0.0
