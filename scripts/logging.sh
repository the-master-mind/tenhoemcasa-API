#!/bin/bash

LOG_FILE=log/logging.log

docker run -it alaxalves/lecli | cut -d '>' -f 2,3 | cut -d '[' -f 2,4 | cut -d ']' -f 1,2 > $LOG_FILE
